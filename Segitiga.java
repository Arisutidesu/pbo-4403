package PBO.week10;

public class Segitiga extends BangunDatar {
    private int alas;
    private int tinggi;
    private int sisi_1;
    private int sisi_2;
    private int sisi_3;

    public Segitiga(int alas, int tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

    public Segitiga(int sisi_1, int sisi_2, int sisi_3) {
        this.sisi_1 = sisi_1;
        this.sisi_2 = sisi_2;
        this.sisi_3 = sisi_3;
    }

    @Override
    void luas(){
        int luas = (alas * tinggi)/2;
        System.out.println("Luas Segitiga: " + luas);
    }

    void keliling(){
        int keliling = sisi_1 + sisi_2 + sisi_3;
        System.out.println("Keliling Segitiga: " + keliling);
    }
}