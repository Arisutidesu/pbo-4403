package PBO.week10;

public class Lingkaran extends BangunDatar {
    private static final double pi = 3.14;
    private double radius;

    Lingkaran(double radius) {
        this.radius = radius;
    }

    @Override
    void luas(){
        double luas = pi * radius * radius;
        System.out.println("Luas Lingkaran: " + luas);
    }

    void keliling(){ 
        double keliling = 2 * pi * radius;
        System.out.println("Keliling Lingkaran: " + keliling);
    }
}