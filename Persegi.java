package PBO.week10;

public class Persegi extends BangunDatar {
    private int sisi;

    public Persegi(int sisi) {
        this.sisi = sisi;
    }

    @Override
    void luas(){
        int luas = sisi * sisi;
        System.out.println("Luas Persegi: " + luas);
    }

    void keliling(){
        int keliling = 4 * sisi;
        System.out.println("Keliling Segitiga: " + keliling);
    }
}
