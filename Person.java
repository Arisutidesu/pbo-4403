package PBO;
import java.util.Scanner;

public class Person {
	protected String name;
	protected String address;
	protected String hobi;
	
	//Default Constructor
	public Person() {
		System.out.println("Inside Person:Constructor");
		name="";
		address="";
		hobi="";
	}
	
	//Constructor dengan parameter
	public Person(String name, String address, String hobi) {
		this.name = name;
		this.address = address;
		this.hobi = hobi;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getHobi() {
		return hobi;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public void setHobi(String hobi) {
		this.hobi = hobi;
	}
	
	public void job(String job)
	{
		System.out.println("Pekerjaan "+job);
	}
	
	public void identity()
	{
		System.out.println("Nama : "+name);
		System.out.println("Alamat : "+address);
		System.out.println("Hobi : "+hobi);
	}

	public void isiIdentity(Scanner scanner){
		System.out.println("Nama : ");
		name = scanner.nextLine();
		System.out.println("Alamat : ");
		address = scanner.nextLine();
		System.out.println("Hobi : ");
		hobi = scanner.nextLine();
	}
}
