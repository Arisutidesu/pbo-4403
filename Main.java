package PBO.week10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan sisi persegi: ");
        int sisi = scanner.nextInt();
        BangunDatar persegi = new Persegi(sisi);
        persegi.luas();
        persegi.keliling();

        System.out.print("Masukkan radius lingkaran: ");
        double radius = scanner.nextDouble();
        BangunDatar lingkaran = new Lingkaran(radius);
        lingkaran.luas();
        lingkaran.keliling();

        System.out.print("Masukkan alas segitiga: ");
        int alas = scanner.nextInt();
        System.out.print("Masukkan tinggi segitiga: ");
        int tinggi = scanner.nextInt();
        BangunDatar segitiga = new Segitiga(alas, tinggi);
        segitiga.luas();
        System.out.print("Masukkan sisi pertama segitiga: ");
        int sisi_1 = scanner.nextInt();
        System.out.print("Masukkan sisi kedua segitiga: ");
        int sisi_2 = scanner.nextInt();
        System.out.print("Masukkan sisi ketiga segitiga: ");
        int sisi_3 = scanner.nextInt();
        BangunDatar segi3 = new Segitiga(sisi_1, sisi_2, sisi_3);
        segi3.keliling();

        scanner.close();
    }
}