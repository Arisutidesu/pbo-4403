package PBO;
import java.util.Scanner;

public class Student extends Person{
	private String nim;
	private int spp;
	private int sks;
	private int modul;
	
	public Student() {
		System.out.println("Inside Student:Constructor");
		super.name="Anda";
	}
	
	public String getNim()
	{
		return nim;
	}
	
	public void setNim(String nim){
		this.nim = nim;
	}

	public int getSpp(){
		return spp;
	}

	public void setSpp(int spp){
		this.spp = spp;
	}

	public int getSks(){
		return sks;
	}

	public void setSks(int sks){
		this.sks = sks;
	}

	public int getModul(){
		return modul;
	}

	public void setModul(int modul){
		this.modul = modul;
	}

	@Override
	public void identity()
	{
		System.out.println("NIM : "+getNim());
		System.out.println("Nama : "+super.name);
		System.out.println("Alamat : "+super.address);
		System.out.println("Hobi : "+super.hobi);
		System.out.println("SPP : "+getSpp());
		System.out.println("SKS : "+getSks());
		System.out.println("Modul : "+getModul());
	}
	
	public void hitungPembayaran(){
		int total = spp + sks + modul;
		System.out.println("Total : " + total);
	}
	public void job()
	{
		System.out.println("Pekerjaan : Mahasiswa");
	}

	public void isiNim(Scanner scanner){
		System.out.println("NIM : ");
		nim = scanner.nextLine();
	}
	
	public String getName() {
		System.out.println("Student name: "+name);
		return name;
	}

	@Override
	public void isiIdentity(Scanner scanner){
		isiNim(scanner);
		super.isiIdentity(scanner);
		System.out.println("SPP : ");
		spp = scanner.nextInt();
		System.out.println("SKS : ");
		sks = scanner.nextInt();
		System.out.println("Modul : ");
		modul = scanner.nextInt();
	}
}
